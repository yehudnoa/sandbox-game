CXX = g++
CXXFLAGS = -Wall -pedantic -O3 -std=c++17
LIBFLAGS = -L/usr/lib/x86_64-linux-gnu -lSDL2 -lSDL2_image -lSDL2_ttf
_DEPS =
_OBJ = main.o Gameloop.o TextureManeger.o Map.o Collision.o
OBJDIR = obj
SRCDIR = src

DEPS = $(patsubst %,$(SRCDIR)/%,$(_DEPS))
OBJ = $(patsubst %,$(OBJDIR)/%,$(_OBJ))

all : compile doc run

$(OBJDIR):
	mkdir $(OBJDIR)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp $(DEPS) $(OBJDIR)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

compile: $(OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LIBFLAGS) -o yehudnoa

doc:
	mkdir doc
	doxygen Doxyfile

clean:
	rm -f yehudnoa
	rm -rf doc
	rm -rf $(OBJDIR)

run:
	./yehudnoa
