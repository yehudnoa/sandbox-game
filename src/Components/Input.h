//
// Created by nikki on 4/27/20.
//

#ifndef GAME_INPUT_H
#define GAME_INPUT_H


#include <SDL2/SDL.h>
#include "../Gameloop.h"
#include "../ECS/Components.h"


///Keyboard control component.
///
///Gives entity abilityto be controled by the users keyboard.
///@authors Noam Nikki Yehuda inspired by BirchEngine
class input : public Component {
public:
    PositionComponent *position;

    void init() override {
        position = &entity->getComponent<PositionComponent>();
    }

    ///Checks keyboard activity and effects entities position accordingly.
    void update() override {
        if(gameloop::event.type == SDL_KEYDOWN){
            switch (gameloop::event.key.keysym.sym){
                case SDLK_UP:
                    position->velocity.y = -1;
                    break;
                case SDLK_LEFT:
                    position->velocity.x = -1;
                    break;
                case SDLK_RIGHT:
                    position->velocity.x = 1;
                    break;
                case SDLK_DOWN:
                    position->velocity.y = 1;
                    break;
                default:
                    break;
            }

        }

        if(gameloop::event.type == SDL_KEYUP){
            switch (gameloop::event.key.keysym.sym){
                case SDLK_UP:
                    position->velocity.y = 0;
                    break;
                case SDLK_LEFT:
                    position->velocity.x = 0;
                    break;
                case SDLK_RIGHT:
                    position->velocity.x = 0;
                    break;
                case SDLK_DOWN:
                    position->velocity.y = 0;
                    break;
                default:
                    break;
            }
        }
    }
};


#endif //GAME_INPUT_H
