//
// Created by nikki on 5/5/20.
//

#ifndef GAME_VECTOR2D_H
#define GAME_VECTOR2D_H
#include <iostream>

///Holds the x and y movements of moving entitys.
///@authors Noam Nikki Yehuda inspired by BirchEngine
class Vector2D{
public:
    float x;
    float y;

    ///Constructor
    Vector2D(){
        x = 0.0f;
        y = 0.0f;
    };

    ///Constructor that takes in x and y
    Vector2D(float x, float y){
        this->x = x;
        this->y = y;
    }
};

#endif //GAME_VECTOR2D_H
