//
// Created by nikki on 5/5/20.
//

#ifndef GAME_COLIDERCOMP_H
#define GAME_COLIDERCOMP_H

//#include "SDL2/SDL.h"
#include "../ECS/Components.h"
#include <string>

///Gives the entity the a rectangle around it so it can detect collition with other entities.
///
///This component 'creates' an invisible rectangle around the entity that can be used in
///the AABB function in the collision class so we can check if it overlaps with another
///entity.
///@authors Noam Nikki Yehuda inspired by BirchEngine
class ColliderComp : public Component {
public:
    SDL_Rect collider;

    PositionComponent* position;

    ColliderComp() = default;

    void init() override{
        position = &entity-> getComponent<PositionComponent>();
    }

    void update() override {
        collider.x = static_cast<int>(position->pos.x);
        collider.y = static_cast<int>(position->pos.y);
        collider.w = position->width * position->scale;
        collider.h = position->hight * position->scale;
    }
};

#endif //GAME_COLIDERCOMP_H
