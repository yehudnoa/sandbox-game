//
// Created by nikki on 6/6/20.
//

#ifndef GAME_TILECOMP_H
#define GAME_TILECOMP_H

#include "../ECS/ECSm.h"
#include "../ECS/Components.h"

///Component that gives Map tiles there qualities
///@authors Noam Nikki Yehuda inspired by BirchEngine
class TileComp : public Component{
public:
    PositionComponent *position;
    SpriteComponent *sprite;
    Vector2D pos;

    SDL_Rect tileR;
    const char* path;

    TileComp() = default;

    /*!Constructor.
     *
     * Gives the tile the correct PNG file based on the id number of the tile.
     * @param x X position of tile.
     * @param y Y position of tile
     * @param id Tiles id.
     */
    TileComp(int x, int y, int id){
        pos.x = x;
        pos.y = y;

        tileR.x = x;
        tileR.y = y;

        switch (id) {
            case 0:
                path = "assets/tile1.png";
                break;
            case 1:
                path = "assets/tile2.png";
                break;
            case 2:
                path = "assets/tile3.png";
                break;
            case 3:
                path = "assets/tile4.png";
                break;
            case 4:
                path = "assets/tile5.png";
                break;
            case 5:
                path = "assets/tile6.png";
                break;
            case 6:
                path = "assets/tile7.png";
                break;
            case 7:
                path = "assets/tile8.png";
                break;
            case 8:
                path = "assets/tile9.png";
                break;
            case 9:
                path = "assets/tile10.png";
                break;
            case 10:
                path = "assets/tile11.png";
                break;
            case 11:
                path = "assets/tile12.png";
                break;
            case 12:
                path = "assets/tile13.png";
                break;
            case 13:
                path = "assets/tile14.png";
                break;
            default:
                break;
        }
    }

    /*!Initializes Map tile.
     *
     * Gives tile position, and position component and gives the png file name to the tiles sprite component.
     */
    void init() override {
        entity->addComponent<PositionComponent>((float)tileR.x, (float)tileR.y, 64, 64, 1);
        position = &entity->getComponent<PositionComponent>();
        entity->addComponent<SpriteComponent>(path);
        sprite = &entity->getComponent<SpriteComponent>();
    }

    ///Moves tiles so the screen follows the player.
    void update() override {
        tileR.x = pos.x - gameloop::camara.x;
        tileR.y = pos.y - gameloop::camara.y;
    }
};

#endif //GAME_TILECOMP_H
