//
// Created by nikki on 5/5/20.
//

#ifndef GAME_SPRITECOMP_H
#define GAME_SPRITECOMP_H

#include "../ECS/ECSm.h"
#include "../TextureManeger.h"
#include "../ECS/Components.h"
#include "SDL2/SDL.h"

///This component gives the entity texture so it can be rendered.
///@authors Noam Nikki Yehuda inspired by BirchEngine
class SpriteComponent : public Component{
private:
    PositionComponent *position;
    SDL_Texture  *texture;
    SDL_Rect srcRect, destRect;

public:
    SpriteComponent() = default;

    ///Destructor.
    ~SpriteComponent(){
        SDL_DestroyTexture(texture);
    }

    /// Constructor.
    /// \param path PNG file name for the texture.
    SpriteComponent(const char* path){
        texture = TextureManeger::LoadTexture(path);
    }

    ///Initializes Sprite Component.
    ///
    ///Gives the entity its ability to be seen.
    void init() override {

        position = &entity->getComponent<PositionComponent>();
        srcRect.x = srcRect.y = 0;
        srcRect.h = 64;
        srcRect.w = 64;
        destRect.w = position->width * position->scale;
        destRect.h = position->hight * position->scale;
    }

    ///Changes visual location of entity based on entities location.
    void update() override {
        destRect.x = (int)position->pos.x - gameloop::camara.x;
        destRect.y = (int)position->pos.y - gameloop::camara.y;
    }

    ///Adds the texture to the render.
    void draw() override {
        TextureManeger::Draw(texture, srcRect, destRect);
    }
};

#endif //GAME_SPRITECOMP_H
