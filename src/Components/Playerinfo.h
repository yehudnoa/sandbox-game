//
// Created by nikki on 5/6/20.
//

#ifndef GAME_PLAYERINFO_H
#define GAME_PLAYERINFO_H

#include "SDL2/SDL_image.h"
#include "../ECS/ECSm.h"
#include "../ECS/Components.h"

///Players info component.
///
///Gives entity player qualities such as health, lives, and score.
///@author Noam Nikki Yehuda
class playerinfo : public Component {
public:
    int lives = 3;
    int health = 100;
    int attack = 100;
    int hiscore = 0;

    ///Checks if player still has health/life left
    void update() override {
        if(health <= 0 && lives == 1){
            entity->destroy();
        }
        if(health <= 0){
            lives -= 1;
            health = 100;
            hiscore = 0;
        }
    }
};

#endif //GAME_PLAYERINFO_H
