//
// Created by nikki on 5/9/20.
//

#ifndef GAME_ATTACKINFO_H
#define GAME_ATTACKINFO_H
#include "SDL2/SDL_image.h"
#include "../ECS/ECSm.h"
#include "../ECS/Components.h"

///Attack component for projectile.
///
///This component helps an entity that moves 35 places in a direction
///and then gets distroyed.
///@author Noam Nikki Yehuda
class attackinfo : public Component {
public:
    int att = 0, moves = 0, x = 1, y = 0;
    PositionComponent *position;

    ///Constructor
    attackinfo(int attack, int x, int y){
        att = attack;
        this->x = x;
        this->y = y;
    }

    ///Gives entity sprite component, collider component, and adds it into Attack group.
    void init() override {
        position = &entity->getComponent<PositionComponent>();
        entity->addComponent<SpriteComponent>("assets/spear.png");
        entity->addComponent<ColliderComp>();
        entity->addGroup(gameloop::groupAttack);
    }

    ///Keeps track of distance entity traveled and when it should be destroyed.
    void update() override {
        if(moves < 35){
            position->velocity.x = x;
            position->velocity.y = y;
            moves += 1;
        } else {
            entity->destroy();
        }

    }
};

#endif //GAME_ATTACKINFO_H
