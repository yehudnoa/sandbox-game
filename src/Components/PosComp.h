//
// Created by nikki on 5/5/20.
//

#ifndef GAME_POSCOMP_H
#define GAME_POSCOMP_H

#include "../ECS/ECSm.h"
#include "Vector2D.h"

///Keeps track of position of entity
///@authors Noam Nikki Yehuda inspired by BirchEngine
class PositionComponent : public Component{
public:
    Vector2D pos;
    Vector2D velocity;

    int hight = 32;
    int width = 32;
    int scale = 1;

    int speed = 6;

    ///Constroctor
    PositionComponent(){
        pos.x = 0.0f;
        pos.y = 0.0f;
    }

    ///Constroctor where you can insert the X and Y position of the entity.
    PositionComponent(float x, float y){
        pos.x = x;
        pos.y = y;
    }

    ///Constroctor where you can insert the X, Y, and scale position of the entity.
    PositionComponent(float x, float y, int sc){
        pos.x = x;
        pos.y = y;
        scale = sc;
    }

    ///Constroctor where you can insert the X, Y, hight, width, and scale position of the entity.
    PositionComponent(float x, float y, int h, int w, int sc){
        pos.x = x;
        pos.y = y;
        hight = h;
        width = w;
        scale = sc;
    }


    void init() override{
        velocity.x = 0;
        velocity.y = 0;
    }

    ///Updates players position based on its velocity.
    void update() override{
        pos.x += velocity.x * speed;
        if(pos.x < 0){
            pos.x = 0;
        }else if(pos.x > 3000){
            pos.x = 3000;
        }
        pos.y += velocity.y * speed;
        if(pos.y < 0){
            pos.y = 0;
        }else if(pos.y > 3100){
            pos.y = 3100;
        }
    }

    ///If collition sent here for entity to bounce off object
    void col() {
        if (velocity.x == -1){
            pos.x += 32;
        }else if(velocity.x == 1){
            pos.x -= 32;
        }
        if (velocity.y == -1){
            pos.y += 32;
        }else if(velocity.y == 1){
            pos.y -= 32;
        }
    }

};

#endif //GAME_POSCOMP_H
