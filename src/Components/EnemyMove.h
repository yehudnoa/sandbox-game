//
// Created by nikki on 5/8/20.
//

#ifndef GAME_ENEMYMOVE_H
#define GAME_ENEMYMOVE_H

#include "../ECS/Components.h"
#include "../ECS/ECSm.h"
#include "../Gameloop.h"


///This component gives the enemies their moving and health aspects.
///@author Noam Nikki Yehuda
class move : public Component {

    PositionComponent *position;
    Manager *manager;
public:
    int health = 100;

    move() = default;

    ///Starts the entity moving.
    void init() override {
        position = &entity->getComponent<PositionComponent>();
        entity->getComponent<PositionComponent>().velocity.y = 1;
    }

    ///Makes sure enemy should still be alive and that the entity hasn't his map border.
    void update() override {
        if(health <= 0){
            entity->destroy();
        }
        if(position->pos.y == 0){
            change();
        }
    }

    ///When the function is called the entity changes direction.
    void change(){
        position->velocity.y *= -1;
    }



};

#endif //GAME_ENEMYMOVE_H
