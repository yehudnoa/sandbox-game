//
// Created by nikki on 5/3/20.
//

#ifndef GAME_ESC_H
#define GAME_ESC_H

#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <bitset>
#include <array>

///@authors birchengine

class Component;
class Entity;
class Manager;

using ComponentID = std::size_t;
using Group = std::size_t;

/*!This inline function returns the next unsigned number in the list.
 */
inline ComponentID getNewComponentTypeID()
{
    static ComponentID lastID = 0u;
    return lastID++;
}

/*!Gives component there ID number.
 *
 * This inline function first makes sure that what is being taken in is a child class of the Components class.
 * Then it gets an id number for the component.
 * @tparam T the new component that should be added to the entity.
 * @return The new components new id number.
 */
template <typename T> inline ComponentID getComponentTypeID() noexcept
{
    static_assert (std::is_base_of<Component, T>::value, "Not component child class");
    static ComponentID typeID = getNewComponentTypeID();
    return typeID;
}

///@param maxComponents max amount of Components per entity
constexpr std::size_t maxComponents = 32;
///@param maxGroups max amount of groups per manager
constexpr std::size_t maxGroups = 32;

///@param ComponentArray array of existing components in the entity
using ComponentArray = std::array<Component*, maxComponents>;

///Parent class
class Component
{
public:
    Entity* entity;

    virtual void init() {}
    virtual void update() {}
    virtual void draw() {}
    virtual ~Component() {}
};

///Game objects
class Entity
{
private:
    Manager& manager;
    ///@param active This bool is to keep track if the entity is still active.
    bool active = true;
    ///@param components A vector of unique pointers of the entities' components.
    std::vector<std::unique_ptr<Component>> components;

    ///@param componentArray array of existing components in the entity
    ComponentArray componentArray;

public:
    ///Constructor.
    Entity(Manager& mManager) : manager(mManager) {}

    ///This function goes through all components of the entity and updates them
    void update()
    {
        for (auto& c : components) c->update();
    }

    ///This function goes through all of entitys' components draw function.
    void draw()
    {
        for (auto& c : components) c->draw();
    }

    /// Checks if entity is still active.
    /// \return if active is true or false.
    bool isActive() const { return active; }
    ///Makes entity not active
    void destroy() { active = false; }

    ///Adds entity to group
    void addGroup(Group mGroup);

    ///Add component to entity.
    ///
    ///Takes in the component and send it to the getComponentTypeID inline function to make sure it is
    ///a child class of the Components class and recive its component ID. Then it 'moves' ( because it is
    ///a unique pointer ) the component into the component vector and adds the component into the componentArray
    ///so it can be located later on when needed.
    /// \return This function returns a pointer to the component.
    ///@attention Do not send variable that is not child class of component class.
    template <typename T, typename... TArgs>
    T& addComponent(TArgs&&... mArgs)
    {
        T* c(new T(std::forward<TArgs>(mArgs)...));
        c->entity = this;
        std::unique_ptr<Component>uPtr { c };
        components.emplace_back(std::move(uPtr));

        componentArray[getComponentTypeID<T>()] = c;

        c->init();
        return *c;
    }

    ///This function looks for component in the componentArray using the components type id.
    ///
    ///@return specific component.
    ///@warning Make sure entity has component already in it.
    template<typename T> T& getComponent() const
    {
        auto ptr(componentArray[getComponentTypeID<T>()]);
        return *static_cast<T*>(ptr);
    }
};

///Manager of all game entities & groups
class Manager{
public:
    ///@param entities Unique pointers vector of all entities in manager.
    std::vector<std::unique_ptr<Entity>> entities;
    ///@param groupEntities vector of vectors that hold the entities in the correct groups
    std::array<std::vector<Entity*>, maxGroups> groupEntities;

    ///@param Player a pointer to the users player entity.
    ///@param Obj a pointer to the levels objective entity.
    Entity * Player = nullptr;
    Entity * Obj = nullptr;

    ///In this function it goes through all the entities updates function in the manager.
    void update(){
        for(auto& e: entities) e->update();
    }

    ///In this function it goes through all the entities draw function in the manager.
    void draw(){
        for (auto& e : entities) e->draw();
    }

    ///In this function it goes through all the entities in the manager and checks if they are still active or if they need to be removed.
    void refresh(){
        for(auto i(0u); i < maxGroups; i++){
            auto& v(groupEntities[i]);
            v.erase(std::remove_if(std::begin(v), std::end(v),
                    [i](Entity* mEntity)
                    {
                        return !mEntity->isActive();
                    }),
                            std::end(v));
        }
    }

    ///This function adds entity to group using group number.
    void AddToGroup(Entity* mEntity, Group mGroup){
        groupEntities[mGroup].emplace_back(mEntity);
    }

    ///This function returns group using the group number
    std::vector<Entity*>& getGroup(Group mGroup)
    {
        return groupEntities[mGroup];
    }

    ///This function adds entity to vector by 'moveing' ( because it is
    ///a unique pointer ) the entity to the vector
    Entity& addEntity(){
        Entity * e = new Entity(*this);
        std::unique_ptr<Entity> uPtr(e);
        entities.emplace_back(std::move(uPtr));
        return *e;
    }
};

#endif //GAME_ESC_H
