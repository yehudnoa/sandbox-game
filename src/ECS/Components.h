//
// Created by nikki on 5/5/20.
//
#ifndef GAME_COMPONENTS_H
#define GAME_COMPONENTS_H

#include "ECSm.h"
#include "../Components/PosComp.h"
#include "../Components/SpriteComp.h"
#include "../Components/Input.h"
#include "../Components/Colidercomp.h"
#include "../Components/Playerinfo.h"
#include "../Components/EnemyMove.h"
#include "../Components/Attackinfo.h"
#include "../Components/Tilecomp.h"

///extention to ECS header

class Components;

#include "ECSm.h"

void Entity::addGroup(Group mGroup)
{
    manager.AddToGroup(this, mGroup);
}

#endif



