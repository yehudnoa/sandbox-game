#include "Gameloop.h"
#include <fstream>

    gameloop *game = nullptr;

int main() {
    const int FPS = 30;
    const int frameDelay = 50 / FPS;
    int FrameCount = 0, SecCount = 0;

    Uint32 frameStart;
    int frameTime;

    game = new gameloop("Back to th Past", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 2000, 1600);
    while(game->Pause()) {
        while (game->running()) {
            frameStart = SDL_GetTicks();

            game->menu();

            frameTime = SDL_GetTicks() - frameStart;
            if (frameDelay > frameTime) {
                SDL_Delay(frameDelay - frameTime);
            }
            if (FrameCount == 60) {
                FrameCount = 0;
                SecCount += 1;
            }

            FrameCount++;
        }
        if(game->Pause()) {
        game->init();
            while (game->running()) {
                frameStart = SDL_GetTicks();

                game->Loop();

                frameTime = SDL_GetTicks() - frameStart;

                if (frameDelay > frameTime) {
                    SDL_Delay(frameDelay - frameTime);
                }
                if (FrameCount == 60) {
                    FrameCount = 0;
                    SecCount += 1;
                }

                FrameCount++;
            }
        }
        while (game->winning()) {
            frameStart = SDL_GetTicks();

            game->WinWindow();

            frameTime = SDL_GetTicks() - frameStart;

            if (frameDelay > frameTime) {
                SDL_Delay(frameDelay - frameTime);
            }

            if (FrameCount == 60) {
                FrameCount = 0;
                SecCount += 1;

            }

            FrameCount++;
        }
        while (!game->Living()) {
            frameStart = SDL_GetTicks();

            game->WinDie();

            frameTime = SDL_GetTicks() - frameStart;

            if (frameDelay > frameTime) {
                SDL_Delay(frameDelay - frameTime);
            }
            if (FrameCount == 60) {
                FrameCount = 0;
                SecCount += 1;
            }

            FrameCount++;
        }
        if (game->level != 4) {
            std::ofstream ofs("test.txt", std::ofstream::trunc);
            ofs << game->level << "\n" << game->h << "\n" << game->l << "\n" << game->s;
            ofs.close();
        }
    }

    delete game;

    return 0;
}