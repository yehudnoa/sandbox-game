//
// Created by nikki on 4/27/20.
//

#ifndef GAME_GAMELOOP_H
#define GAME_GAMELOOP_H

#include <SDL2/SDL_image.h>
#include "ECS/ECSm.h"
#include <SDL2/SDL_ttf.h>
#include <vector>

///This class controls the entire game.
 /*! Here contains the Menu and Game
 * @author Noam Nikki Yehuda
 */
class gameloop {
public:
    /*!Constructor function.
     *
     *The function creates a window that the rest of the game will be rendered into.
     * @param title The title that will be on the window.
     * @param width The width of the wondow.
     * @param hight The hight of the window.
     */
    gameloop(const char* title, int xpos, int ypos, int width, int hight);

    /*!Main Menu of game.
     *
     *Here the user decides if he wants to start a new game ( by pressing 1 ), go to a saved game
     * ( by pressing 2 ), which takes the information from the text file ( level, health, lives, score )
     * and puts it in the game. You can also go to the introduction page ( by pressing 3 ) there you
     * will see a bit of a backstory and how to play the game.
     */
    void menu();

    /*!Window rendered when you win the game.
     *
     * You can press ESCAPE button to return to Main Menu.
     */
    void WinWindow();

    /*!Window rendered when you die in the game.
     *
     * You can press ESCAPE button to return to Main Menu.
     */
    void WinDie();

    ///Destructor funstion.
    ~gameloop();

    /*!Creates the game itself.
     *
     * Here we initualize everything to start the game at the designated level. All bools and SDL_Rect's
     * are put back to there original inputs. A player is created and all the attributes are put in, such
     * as health, lives, and score. After that the second layer of the level is put into the manger to be rendered.
     */
    void init();

    /*!Loop of every frame of the game.
     *
     *30 times a second the game renders a new frame. For every frame the game most go through this function
     * to know the current status of the game. ( Game objects positions, what is destroyed on that frame and should
     * no longer be renders, and everything in between. )
     */
    void Loop();

    /*!Player makes an attack.
     *
     *When this funstion is called it creates a new entity of attack that traviles in the direction of the players
     * current or last movement (horizontal movements are favored over vertical).
     * @see Loop called from Loop when spacebar is hit.
     */
    void attack();

    /*!Moves game to next level of the game.
     *
     *When function is called it first delete all the entities in the manager then it checks what level is next
     * ( if game is at the end it will end the game and make Win bool true ) and renders the new level into the
     * games.
     * @see Loop called when player collides with level objective.
     */
    void nextlevel();

    /*!Clears all entities.
     *
     *@see Loop called when exits game.
     * @see nextlevel called when exits game.
     */
    void Clear();

    /*!If the game is still Running.
     *
     * @return if isRunning is true or false.
     */
    bool running(){ return isRunning;}

    /*!If game is Paused.
     *
     * If the user wants to return to Main Menu.
     * @return if Run is true or false.
     */
    bool Pause(){
        if (!Run) return Run;
        isRunning = true;
        return Run;}

    /*!If the user won the game.
    *
    * @return if Win is true or false.
    */
    bool winning(){ return Win;}

    /*!If the user died in the game.
    *
    * @return if Living is true or false.
    */
    bool Living(){ return Alive;}

    /*!Creates tile entity.
     *
     * @param xpos X position of the tile.
     * @param ypos Y position of the tile.
     * @param id what type of tile it is.
     * @see Map
     */
    static void TileMap(int xpos, int ypos, int id);

    /*!Creates physical layer of the level.
     *
     * Takes in the 2d int array and sends all non-zero numbers into Switch function to render game objects in the right
     * place.
     * @param tmp 2d int array of represents the physical layer of level.
     */
    static void AddTile(int tmp[50][50]);

    /*!Creates correct entity in correct places.
     *
     * Depending on the id numbers the correct components are added to the entity and puts it in the correct position
     * @param x X postion of entity.
     * @param y Y position of entity.
     * @param id What type of entity is it.
     * @see AddTile.
     */
    static void Switch(int x, int y, int id);

    /*!Checks If entities are colliding.
     *
     * In the function it goes through different conditions and checks if they occurred.
     * @see Loop called in every frame.
     */
    void Check();

    /*!
     * @param a rectangle that is around the player that lets the screen move when the player does.
     * @param directionx the x axes direction the player made last.
     * @param directiony the y axes direction the player made last.
     * @param level the current level that the user is on.
     * @param groupLabels assosiates group name with a size_t number so when using the name it actually stores the entity under that group number
     */
    static SDL_Renderer* _renderer;
    static SDL_Event event;
    static SDL_Rect camara;
    int directionx, directiony, level = 1, h = 100, s = 0, l = 3;

    //assosiates group name with a size_t number
    enum groupLabels : std::size_t {
        groupMap,       //0u
        groupPlayers,   //1u
        groupEnemies,   //2u
        groupAttack,    //3u
        groupPowerups,  //4u
        groupObjective,  //5u
        groupBorder,
        groupTiles,
        extra
    };
private:
    bool isRunning;
    bool Win = false;
    bool Alive = true;
    bool Run = true;
    int cnt = 0;
    SDL_Window* _window;
};


#endif //GAME_GAMELOOP_H
