//
// Created by nikki on 4/27/20.
//

#include "Gameloop.h"
#include "Map.h"
#include "ECS/Components.h"
#include "Components/Vector2D.h"
#include "Collision.h"
#include <sstream>
#include <fstream>

//////////////////////////////////////////layer 2 levels////////////////////////////////////////////////////

int play1[50][50]{
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,2,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,3,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,2,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,2,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,4,4,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,5,0,1,0,0,0,0,0,1,0,0,0,0,0,2,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,4,4,4,4,4,4,4,4,4,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,4,4,4,4,4,4,4,4,4,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,2,0,0,0,1,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,2,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,1,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
        {4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
};

int play2[50][50] = {
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,2,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,2,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,4,4,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,4,4,4,4,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,4,4,4,4,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0},
        {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1},
        {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,3,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

int play3[50][50] = {
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,1,1,0,0,0,0,0,0,2,0,0,0,0,0,4,4,4,4,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,1},
        {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,1},
        {0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,4,4,1},
        {0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,4,4,1},
        {0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,1,0,3,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,2,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,3,0,1,0,0,0,0,0,0,0,0,1,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,1},
        {0,0,0,0,0,0,0,1,0,0,0,0,1,1,1,1,1,1,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,2,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,1,1,0,3,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,2,0,0,0,1,1,1,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,2,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,3,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,1},
        {0,4,4,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
};

SDL_Renderer * gameloop::_renderer = nullptr;
Map* map;
SDL_Event gameloop::event;
Manager manager;
SDL_Rect gameloop::camara = {0,0,2000,1700}, Rect1, Rect2, Rect3, boredR = {1700, 0, 300, 1700};
SDL_Texture * Text1, *Text2, *Text3;


//creates window
gameloop::gameloop(const char *title, int xpos, int ypos, int width, int hight) {
    if(SDL_Init(SDL_INIT_EVERYTHING) == 0){
        _window = SDL_CreateWindow(title, xpos, ypos, width, hight, 0);
        _renderer = SDL_CreateRenderer(_window, -1, 0);
        if(_renderer){
            SDL_SetRenderDrawColor(_renderer, 100,225,120, 255);
        }
        isRunning = true;
        TTF_Init();
    } else {
        isRunning = false;
    }
}

gameloop::~gameloop() {
    SDL_DestroyTexture(Text1);
    SDL_DestroyTexture(Text2);
    SDL_DestroyTexture(Text3);
    delete map;
    SDL_DestroyWindow(_window);
    SDL_DestroyRenderer(_renderer);
    TTF_Quit();
    SDL_Quit();
}

//menu page
void gameloop::menu() {
    SDL_Texture * end = TextureManeger::LoadTexture("assets/background.png");
    SDL_Texture * intro = TextureManeger::LoadTexture("assets/intro.png");
    SDL_Rect R = {0,0,2000,1700};
    bool T = true;
    int i = 0, tmp;
    std::ifstream file("test.txt");
    std::string str;

    SDL_PollEvent(&event);
    if(event.type == SDL_QUIT){
        isRunning = false;
        Run = false;
    }

    if(event.type == SDL_KEYDOWN){
        switch (event.key.keysym.sym) {
            case SDLK_1:
                level = 1;
                h = 100;
                s = 0;
                l = 3;
                isRunning = false;
                break;
            case SDLK_2:
                //puts info from file into game
                while (std::getline(file, str)) {
                    if (i == 0) {
                        if (sscanf(str.c_str(), "%d", &tmp) != 1) {
                            std::cout << "ERROR\n";
                        } else {
                            gameloop::level = tmp;
                        }
                    } else if (i == 1) {
                        if (sscanf(str.c_str(), "%d", &tmp) != 1) {
                            std::cout << "ERROR\n";
                        } else {
                            gameloop::h = tmp;
                        }
                    } else if (i == 2) {
                        if (sscanf(str.c_str(), "%d", &tmp) != 1) {
                            std::cout << "ERROR\n";
                        } else {
                            gameloop::l = tmp;
                        }
                    } else if (i == 3) {
                        if (sscanf(str.c_str(), "%d", &tmp) != 1) {
                            std::cout << "ERROR\n";
                        } else {
                            gameloop::s = tmp;
                        }
                    }
                    i++;
                }
                isRunning = false;
                break;
            case SDLK_3:
                while (T) {
                    SDL_PollEvent(&event);
                    if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) {
                        T = false;
                    }
                    if(event.type == SDL_QUIT){
                        T = false;
                        isRunning = false;
                        Run = false;
                    }
                    SDL_RenderClear(_renderer);
                    SDL_RenderCopy(_renderer, intro, NULL, &R);
                    SDL_RenderPresent(_renderer);
                }
                break;
            default:
                break;
        }
    }

    SDL_RenderClear(_renderer);
    SDL_RenderCopy(_renderer,end, NULL, &R);
    SDL_RenderPresent(_renderer);
    SDL_DestroyTexture(end);
    SDL_DestroyTexture(intro);
}

//start game render
void gameloop::init(){
    isRunning = true;
    Alive = true;
    Win = false;
    Rect1.x = 1720; Rect1.y = 500;
    Rect2.x = 1720; Rect2.y = 600;
    Rect3.x = 1720; Rect3.y = 700;

    map = new Map;
    map->LoadMap(level);

    //creates player with all its attributes
    auto& Player(manager.addEntity());
    manager.Player = &Player;
    Player.addComponent<PositionComponent>(0, 150, 2);
    Player.addComponent<SpriteComponent>("assets/player.png");
    Player.addComponent<input>();
    Player.addComponent<ColliderComp>();
    Player.addComponent<playerinfo>();
    Player.addGroup(groupPlayers);
    Player.getComponent<playerinfo>().health = h;
    Player.getComponent<playerinfo>().lives = l;
    Player.getComponent<playerinfo>().hiscore = s;

    //physical layer of game
    if(level == 1){
        AddTile(play1);
    }else if(level == 2){
        AddTile(play2);
    }else if(level == 3){
        AddTile(play3);
    }
}

//the loop for every frame
void gameloop::Loop() {
    //text update
    SDL_Texture * box = TextureManeger::LoadTexture("assets/blank.png");
    std::ostringstream tmp;
    tmp<<"LIVES:"<<manager.Player->getComponent<playerinfo>().lives;
    Text1 = TextureManeger::textonscreen(tmp.str().c_str(),gameloop::_renderer,{0,0,0,255},&Rect1);
    tmp.str("");
    tmp.clear();
    tmp<<"HEALTH:"<<manager.Player->getComponent<playerinfo>().health;
    Text2 = TextureManeger::textonscreen(tmp.str().c_str(),gameloop::_renderer,{0,0,0,255},&Rect2);
    tmp.str("");
    tmp.clear();
    tmp<<"SCORE:"<<manager.Player->getComponent<playerinfo>().hiscore;
    Text3 = TextureManeger::textonscreen(tmp.str().c_str(),gameloop::_renderer,{0,0,0,255},&Rect3);

    //exit program
    SDL_PollEvent(&event);
    switch (event.type){
        case SDL_QUIT:
            isRunning = false;
            Run = false;
            Clear();
            break;
        default:
            break;
    }

    if(event.type == SDL_KEYDOWN){
        switch (event.key.keysym.sym){
            case SDLK_ESCAPE:
                isRunning = false;
                Clear();
                break;
            case SDLK_SPACE:
                attack();
                break;
            default:
                break;
        }
    }

    manager.update();
    Check();
    manager.refresh();


    camara.x = manager.Player->getComponent<PositionComponent>().pos.x - 850;
    camara.y = manager.Player->getComponent<PositionComponent>().pos.y - 850;
    if(camara.x < 0){
        camara.x = 0;
    }
    if(camara.y < 0){
        camara.y = 0;
    }
    if(camara.x > camara.w){
        camara.x = camara.w;
    }
    if(camara.y > camara.h){
        camara.y = camara.h;
    }

    //gets direction of movement for attack
    if(manager.Player->getComponent<PositionComponent>().velocity.y != 0){
        directiony = manager.Player->getComponent<PositionComponent>().velocity.y;
        directionx = 0;
    }
    if(manager.Player->getComponent<PositionComponent>().velocity.x != 0){
        directionx = manager.Player->getComponent<PositionComponent>().velocity.x;
        directiony = 0;
    }

    //if player has reached objective to go to next level
    if (Collision::AABB(manager.Player->getComponent<ColliderComp>().collider,
            manager.Obj->getComponent<ColliderComp>().collider)) {
        manager.Player->getComponent<playerinfo>().hiscore += 500;
        manager.Obj->destroy();
        level += 1;
        nextlevel();
    }
    if(isRunning && manager.getGroup(groupPlayers).size() == 0){
        Alive = false;
        isRunning = false;
        Clear();
        manager.refresh();
    }

    SDL_RenderClear(_renderer);
    for (auto& t : manager.getGroup(groupTiles)){ t->draw();}
    for (auto& t : manager.getGroup(groupPowerups)){ t->draw();}
    for (auto& t : manager.getGroup(groupEnemies)){ t->draw();}
    for (auto& t : manager.getGroup(groupMap)){ t->draw();}
    for (auto& t : manager.getGroup(groupObjective)){ t->draw();}
    for (auto& t : manager.getGroup(groupPlayers)){ t->draw();}
    for (auto& t : manager.getGroup(groupAttack)){ t->draw();}
    SDL_RenderCopy(_renderer, box, NULL, &boredR);
    SDL_RenderCopy(_renderer ,Text1,NULL ,&Rect1 );
    SDL_RenderCopy(_renderer ,Text2,NULL ,&Rect2 );
    SDL_RenderCopy(_renderer ,Text3,NULL ,&Rect3 );
    SDL_RenderPresent(_renderer);

    SDL_DestroyTexture(box);
    SDL_DestroyTexture(Text1);
    SDL_DestroyTexture(Text2);
    SDL_DestroyTexture(Text3);
}

void gameloop::TileMap(int x, int y, int id){
    auto& tile(manager.addEntity());
    tile.addComponent<TileComp>(x, y, id);
    tile.addGroup(groupTiles);
}

void gameloop::AddTile(int tmp[50][50]) {
    int type, x, y;
    for (int row = 0; row < 50; row++){
        for(int col = 0; col < 50; col++) {
            type = tmp[row][col];

            x = col * 64;
            y = row * 64;
            if(type != 0){
                Switch(x, y, type);
            }
        }
    }
}

void gameloop::nextlevel() {
    gameloop::h = manager.Player->getComponent<playerinfo>().health;
    gameloop::s = manager.Player->getComponent<playerinfo>().hiscore;
    gameloop::l = manager.Player->getComponent<playerinfo>().lives;
    for (auto& m : manager.getGroup(groupMap)){ m->destroy();}
    for (auto& e : manager.getGroup(groupEnemies)){ e->destroy();}
    for (auto& p : manager.getGroup(groupPowerups)){ p->destroy();}
    for (auto& b : manager.getGroup(groupBorder)){ b->destroy();}
    for (auto& t : manager.getGroup(groupTiles)){ t->destroy();}
    for (auto& t : manager.getGroup(groupObjective)){ t->destroy();}
    manager.refresh();
    if(level == 2){
        map->LoadMap(2);
        AddTile(play2);
        manager.Player->getComponent<PositionComponent>().pos.x = 0;
        manager.Player->getComponent<PositionComponent>().pos.y = 150;
    }else if(level == 3){
        map->LoadMap(3);
        AddTile(play3);
        manager.Player->getComponent<PositionComponent>().pos.x = 0;
        manager.Player->getComponent<PositionComponent>().pos.y = 150;
    }else{
        Win = true;
        isRunning = false;
        Clear();
        manager.refresh();
    }

}

void gameloop::Switch(int x, int y, int id) {
    auto& tmp(manager.addEntity());
    tmp.addComponent<PositionComponent>(x, y, 2);
    tmp.addComponent<ColliderComp>();
    switch(id){
        case 1:
            tmp.addGroup(gameloop::groupBorder);
            break;
        case 2:
            tmp.addComponent<SpriteComponent>("assets/minotaur.png");
            tmp.addComponent<move>();
            tmp.addGroup(gameloop::groupEnemies);
            break;
        case 3:
            tmp.addComponent<SpriteComponent>("assets/powerup1.png");
            tmp.addGroup(gameloop::groupPowerups);
            break;
        case 4:
            tmp.addComponent<SpriteComponent>("assets/lava.png");
            tmp.addGroup(gameloop::groupMap);
            break;
        case 5:
            tmp.addComponent<SpriteComponent>("assets/screw.png");
            tmp.addGroup(gameloop::groupObjective);
            manager.Obj = &tmp;
            break;
        default:
            tmp.addGroup(gameloop::extra);
            break;
    }
}

void gameloop::Clear() {
    for (auto& m : manager.getGroup(groupMap)){ m->destroy();}
    for (auto& e : manager.getGroup(groupEnemies)){ e->destroy();}
    for (auto& p : manager.getGroup(groupPowerups)){ p->destroy();}
    for (auto& b : manager.getGroup(groupBorder)){ b->destroy();}
    for (auto& t : manager.getGroup(groupTiles)){ t->destroy();}
    for (auto& t : manager.getGroup(groupPlayers)){ t->destroy();}
    for (auto& t : manager.getGroup(groupObjective)){ t->destroy();}
}

void gameloop::Check() {
    for (auto& m : manager.getGroup(groupMap)){
        if (Collision::AABB(manager.Player->getComponent<ColliderComp>().collider, m->getComponent<ColliderComp>().collider)) {
            manager.Player->getComponent<PositionComponent>().col();
            manager.Player->getComponent<playerinfo>().health -= 5;}
    }
    for (auto& m : manager.getGroup(groupBorder)){
        if (Collision::AABB(manager.Player->getComponent<ColliderComp>().collider, m->getComponent<ColliderComp>().collider)) {
            manager.Player->getComponent<PositionComponent>().col();
        }
    }
    //example that if collide with enemies
    for (auto& e : manager.getGroup(groupEnemies)){
        if (Collision::AABB(manager.Player->getComponent<ColliderComp>().collider, e->getComponent<ColliderComp>().collider)) {
            manager.Player->getComponent<PositionComponent>().col();
            manager.Player->getComponent<playerinfo>().health -= 5;
        }
    }
    //get power up
    for (auto& p : manager.getGroup(groupPowerups)) {
        if (Collision::AABB(manager.Player->getComponent<ColliderComp>().collider, p->getComponent<ColliderComp>().collider)) {
            manager.Player->getComponent<playerinfo>().health += 20;
            p->destroy();
        }
    }
    //example of attack and enemy
    for (auto& a : manager.getGroup(groupAttack)) {
        for (auto& e : manager.getGroup(groupEnemies)){
            if (Collision::AABB(a->getComponent<ColliderComp>().collider, e->getComponent<ColliderComp>().collider)) {
                e->getComponent<move>().health -= (0.5 * manager.Player->getComponent<playerinfo>().health);
                manager.Player->getComponent<playerinfo>().hiscore += 25;
                a->destroy();
            }
        }
    }
    for (auto& e : manager.getGroup(groupEnemies)){
        for (auto& m : manager.getGroup(groupBorder)) {
            if (Collision::AABB(m->getComponent<ColliderComp>().collider, e->getComponent<ColliderComp>().collider)) {
                e->getComponent<move>().change();
            }
        }
    }

    for (auto& e : manager.getGroup(groupEnemies)){
        for (auto& m : manager.getGroup(groupMap)) {
            if (Collision::AABB(m->getComponent<ColliderComp>().collider, e->getComponent<ColliderComp>().collider)) {
                e->getComponent<move>().change();
            }
        }
    }
}

//create attack
void gameloop::attack(){
    auto& tmp(manager.addEntity());
    tmp.addComponent<PositionComponent>(manager.Player->getComponent<PositionComponent>().pos.x, manager.Player->getComponent<PositionComponent>().pos.y);
    tmp.addComponent<attackinfo>(manager.Player->getComponent<playerinfo>().attack, directionx, directiony);
}


void gameloop::WinWindow() {
    std::ostringstream s;
    SDL_Texture * end = TextureManeger::LoadTexture("assets/win.png");
    s<<"SCORE:"<<manager.Player->getComponent<playerinfo>().hiscore;
    Text3 = TextureManeger::textonscreen(s.str().c_str(),gameloop::_renderer,{0,0,0,255},&Rect3);
    Rect3.x = 800; Rect3.y = 300;
    Rect3.h *= 2; Rect3.w *= 2;
    Rect1.x = 0; Rect1.y = 0;
    Rect1.w = 2000; Rect1.h = 1700;
    SDL_PollEvent(&event);
    if(event.type == SDL_QUIT){
        Win = false;
        Run = false;
    }
    if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) {
        Win = false;
    }
    if(!end){
        std::cout<<"fail"<<std::endl;
    }
    SDL_RenderClear(_renderer);
    SDL_RenderCopy(_renderer, end, NULL, &Rect1);
    SDL_RenderCopy(_renderer, Text3, NULL, &Rect3);
    SDL_RenderPresent(_renderer);
    SDL_DestroyTexture(end);
    SDL_DestroyTexture(Text3);
}

void gameloop::WinDie() {
    SDL_Texture * end = TextureManeger::LoadTexture("assets/died.png");
    Rect1.x = 0; Rect1.y = 0;
    Rect1.w = 2000; Rect1.h = 1600;
    SDL_PollEvent(&event);
    if(event.type == SDL_QUIT){
        Alive = true;
        Run = false;
    }
    if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) {
        Alive = true;
    }
    if(!end){
        std::cout<<"fail"<<std::endl;
    }
    SDL_RenderClear(_renderer);
    SDL_RenderCopy(_renderer,end, NULL, &Rect1);
    SDL_RenderPresent(_renderer);
    SDL_DestroyTexture(end);
}