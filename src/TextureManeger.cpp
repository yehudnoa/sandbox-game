//
// Created by nikki on 5/2/20.
//

#include "TextureManeger.h"
#include "Gameloop.h"

//reneders image on window

SDL_Texture* TextureManeger::LoadTexture(const char *filename) {
    SDL_Surface* tempSurface = IMG_Load(filename);
    SDL_Texture* tex = SDL_CreateTextureFromSurface(gameloop::_renderer, tempSurface);
    SDL_FreeSurface(tempSurface);

    return tex;
}

void TextureManeger::Draw(SDL_Texture *tex, SDL_Rect src, SDL_Rect dest) {
    SDL_RenderCopy(gameloop::_renderer, tex, &src,&dest);
}


SDL_Texture * TextureManeger::textonscreen( const char *message, SDL_Renderer *renderer, const SDL_Colour &color, SDL_Rect *p){
    TTF_Font *font = TTF_OpenFont("assets/sans.ttf",25);
    if(!font){
        std::cout<<"failed\n";
    }
    SDL_Surface* text_surface = TTF_RenderText_Solid(font, message, color);
    p->w = text_surface->clip_rect.w *2;
    p->h = text_surface->clip_rect.h *2;
    SDL_Texture *text_t = SDL_CreateTextureFromSurface(renderer, text_surface);
    TTF_CloseFont(font);
    SDL_FreeSurface(text_surface);
    return text_t;
}