//
// Created by nikki on 5/5/20.
//

#ifndef GAME_COLLISION_H
#define GAME_COLLISION_H

#include "SDL2/SDL.h"

///Detects square collition.
class Collision{
public:
    ///Detects square collition.
    /// \param rectA one square.
    /// \param recB second square.
    /// \return true or false. True means the two square overlap.
    /// @author BirchEngine
    static bool AABB(const SDL_Rect& rectA, const SDL_Rect& recB);
};

#endif //GAME_COLLISION_H
