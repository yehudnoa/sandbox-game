//
// Created by nikki on 5/2/20.
//

#ifndef GAME_TEXTUREMANEGER_H
#define GAME_TEXTUREMANEGER_H

#include "Gameloop.h"

///Handles the textures' of the game.
///@authors Noam Nikki Yehuda inspired by BirchEngine
class TextureManeger {
public:
    /*!Creates texture from the PNG filename.
     *
     * @param filename PNG filename.
     * @return this function returns the created SDL_Texture.
     */
    static SDL_Texture* LoadTexture(const char* filename);

    /*!Puts texture into the render.
     *
     * @param tex the texture that should be added to the render.
     * @param src the part of the texture you want you use.
     * @param dest the size the texture should be on the screen, and where.
     */
    static void Draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest);

    /*!Creates words as texture.
     *
     * @param message the message that you want to be rendered.
     * @param color the color you want the the text to have.
     * @return this function returns the created SDL_Texture.
     */
    static SDL_Texture *textonscreen( const char *message, SDL_Renderer *renderer, const SDL_Colour &color, SDL_Rect * p);
};


#endif //GAME_TEXTUREMANEGER_H
