//
// Created by nikki on 5/3/20.
//

#ifndef GAME_MAP_H
#define GAME_MAP_H

#include "Gameloop.h"

///Creates the Map for the level.
class Map {
public:

    /// Puts current level array into map 2d array.
    /// \param arr level array.
    void GetMap(int arr[50][50]);

    /// Sends tiles with with there id's, and positions to gameloop::TileMap.
    /// \param lvl level that needs to be loaded.
    void LoadMap(int lvl);

private:
    std::string texID;
    int mapScale;
    int tileSize;
    int scaledSize;

    int map[50][50];
};
///@authors Noam Nikki Yehuda inspired by BirchEngine

#endif //GAME_MAP_H
